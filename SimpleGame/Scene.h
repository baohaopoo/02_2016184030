#pragma once
#include"ScnMgr.h"


class Scene 
{

public:
	Scene();
	~Scene();


public:
	virtual void Initialize() = 0;
	virtual void Update() = 0;
	virtual void LateUpdate() = 0;
	virtual void Release() = 0;

};
