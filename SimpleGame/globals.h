#pragma once

#define MAX_OBJ_COUNT 1000
#define HERO_ID 0
#define MONSTER_ID 1
#define KTEMP -10
#define GRAVITY 9.8f
#define OBJ_NORMAL 0.f
#define OBJ_MONSTER 2.f;
#define OBJ_BULLET 1.f
#define LOAD 5
#define START 6
#define END 7
