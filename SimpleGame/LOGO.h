#pragma once
#include"Scene.h"
#include"Renderer.h"
#include"ScnMgr.h"

class LOGO :
	public Scene, public ScnMgr	
{
public:
	LOGO();
	~LOGO();

public:

	virtual void Initialize() override;
	virtual void Update() override;
	virtual void LateUpdate() override;

	virtual void Release() override;


private:

	Renderer* m_Renderer = NULL;
	bool m_KeyR = false;
};

