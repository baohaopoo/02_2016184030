#include "stdafx.h"
#include "ScnMgr.h"
#include "Object.h"
#include "Dependencies\freeglut.h"

int g_testTex = 1;
int g_testAnimTex = 1;
int g_testBack = -1;
//로딩하는 test
int g_testLoading = -1;

int g_testsoundBG = -1;
int g_testsoundFire = -1;
int g_testsoundExplo = -1;


int g_testParticle = -1;
int g_testParticleTex = -1;
ScnMgr::ScnMgr()
{
	// Initialize Renderer
	m_Renderer = new Renderer(500, 120);
	if (!m_Renderer->IsInitialized())
	{
		std::cout << "Renderer could not be initialized.. \n";
	}


	/*if (g_state==LOAD)
	{*/
	if (m_keySP == false)
	{

		m_Start = new Renderer(500, 500);
		if (!m_Renderer->IsInitialized())
		{
			std::cout << "Renderer could not be initialized.. \n";
		}



		g_testLoading = m_Renderer->GenPngTexture("./back0.png");

	}
		


	//}

	//if (g_state == START)
	{
		m_Phy = new Physics();
		m_Sound = new Sound();

		//Initialize objects
		for (int i = 0; i < MAX_OBJ_COUNT; ++i) {
			m_Obj[i] = NULL;
		}

		//영웅 생성 특수의 경우.
		m_Obj[HERO_ID] = new Object();
		m_Obj[HERO_ID]->SetPos(0, -1.32, 0);
		m_Obj[HERO_ID]->SetVel(0, 0, 0);
		m_Obj[HERO_ID]->SetVol(6, 0.7, 2);
		m_Obj[HERO_ID]->SetColor(1, 0, 0, 0);
		m_Obj[HERO_ID]->SetHP(10);
		m_Obj[HERO_ID]->SetMass(1);
		m_Obj[HERO_ID]->SetFricoef(0.7f);
		m_Obj[HERO_ID]->SetType(OBJ_NORMAL);
		

		
		//m_Obj[MONSTER_ID] = new Object();
		//m_Obj[MONSTER_ID]->SetPos(0, -1, 0);
		//m_Obj[MONSTER_ID]->SetVel(0, 0, 0);
		//m_Obj[HERO_ID]->SetVol(0.1, 0.1, 0.1);
		//m_Obj[HERO_ID]->SetColor(1, 0, 0, 1);
		//m_Obj[HERO_ID]->SetHP(10);
		//m_Obj[HERO_ID]->SetMass(1);
		//m_Obj[HERO_ID]->SetFricoef(0.7f);
		//m_Obj[HERO_ID]->SetType(OBJ_NORMAL);


		g_testAnimTex = m_Renderer->GenPngTexture("./onground.png");
		m_Obj[HERO_ID]->SetTexID(g_testAnimTex);


		g_testParticleTex = m_Renderer->GenPngTexture("./part.png");
		g_testParticle = m_Renderer->CreateParticleObject(
			100,
			-250, 250, 250, 250
			, 5, 5,
			7, 7,
			-50, -50, 50, 50);

		g_testBack = m_Renderer->GenPngTexture("./back2.png");
		g_testsoundBG = m_Sound->CreateBGSound("./sound/bgm.mp3");
		m_Sound->PlayBGSound(g_testsoundBG, true, 1.f);
		g_testsoundFire = m_Sound->CreateShortSound("./sound/bullet.mp3");


		int monster = AddObject(
			0, 0, 1.5,
			1, 0, 1,
			1, 0, 0, 0,
			0, 0, 0,
			12,
			0.8,
			0.7,
			OBJ_NORMAL);

		g_testTex = m_Renderer->GenPngTexture("./enemy.png");
		m_Obj[monster]->SetTexID(g_testTex);
	}
}


ScnMgr::~ScnMgr()
{
	if (m_Renderer != NULL)
	{
		delete m_Renderer;
		m_Renderer = NULL;
	}
}

void ScnMgr::DoGarbageCollection()
{

	for (int i = 0; i < MAX_OBJ_COUNT; ++i)
	{
		if (m_Obj[i] == NULL)
			continue;
		int type = -1;
		m_Obj[i]->GetType(&type);

		if (type == OBJ_BULLET)
		{
			float vx, vy, vz;
			m_Obj[i]->GetVel(&vx, &vy, &vz);
			float vSize = sqrtf(vx*vx + vy * vy + vz * vz);
			if (vSize < FLT_EPSILON)
			{
				DeleteObject(i);
				continue;
			}
		}
		/*
		float hp;
		m_Obj[i]->GetHP(&hp);
		if (hp < FLT_EPSILON)
		{
			DeleteObject(i);
		}*/
	}
}

void ScnMgr::RenderScene()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.0f, 0.3f, 0.3f, 1.0f);
	
//	m_Renderer->DrawGround(0, 0, 0, 1500, 570, 0, 1, 1, 1, 1, g_testBack);
	m_Renderer->DrawGround(0, 0, 0, 800, 800, 0, 1, 1, 1, 1, g_testBack);

	//m_Start->DrawGround(0, 0, 0, 800, 800, 0, 1, 1, 1, 1, g_testLoading);



	static float pTime = 0.f;
	pTime += 0.016f;
	m_Renderer->DrawParticle(
		g_testParticle,
		0, 0, 0,
		1,
		1, 1, 1, 1,
		0, 0,
		g_testParticleTex,
		1.f,
		pTime
		);


	// Draw all m_Objs

	for (int i = 0; i < MAX_OBJ_COUNT; ++i) {
		if (m_Obj[i] != NULL) {
			float x, y, z = 0;
			float sx, sy, sz = 0;
			float r, g, b, a = 0;

			m_Obj[i]->GetPos(&x, &y, &z);

			x = 100.f*x;
			y = 100.f*y;
			z = 100.f*z;

			m_Obj[i]->GetVol(&sx, &sy, &sz);

			sx = 100.f*sx;
			sy = 100.f*sy;
			sz = 100.f*sz;

			m_Obj[i]->GetColor(&r, &g, &b, &a);

			m_Renderer->DrawSolidRect(x, y, z, sx, r, g, b, a);

			int texID = -1;
			m_Obj[i]->GetTexID(&texID);
			int type;
			m_Obj[i]->GetType(&type);

			if (type == OBJ_NORMAL)
			{
				if (i == HERO_ID)
				{
					static int ttt = 0;
					m_Renderer->DrawTextureRectAnim(
						x, y, z,
						sx, sy, sz,
						1, 1, 1, 1,
						texID,
						1,
						1,
						ttt,
						0
					);
					ttt++;
					//	ttt = ttt % 6;
				}

			

				else if (type == OBJ_NORMAL )
				{


					
					m_Renderer->DrawSolidRectGauge(
						x, y, z,
						0, sy / 2.f, 0,
						sx, 5, 0,
						1, 0, 0, 1,
						1);
		
				}



			}
			

			else
			{
				m_Renderer->DrawTextureRect(
					x, y, z,
					sx, sy, sz,
					r, g, b, a,
					texID
				);
				m_Renderer->DrawSolidRectGauge(
					x, y, z,
					0, 30, 0,
					sx, 5, 1,
					1, 1, 1, 1,
					100);
			}

			
		}
	}

}


int ScnMgr::AddObject(float x, float y, float z,
	float sx, float sy, float sz,
	float r, float g, float b, float a,
	float vx, float vy, float vz,
	float hp,
	float vmass, 
	float friccoef, 
	int type)
{
	int idx = -1;

	for (int i = 0; i < MAX_OBJ_COUNT; ++i) {
		if (m_Obj[i] == NULL) {
			idx = i;
			break;
		}
	}

	// 예외처리 코드

	if (idx == -1) {
		std::cout << "No more empty obj slot. " << std::endl;	//로그를 띄워준다
		return -1;
	}

	m_Obj[idx] = new Object();
	m_Obj[idx]->SetVel(vx, vy, vz);
	m_Obj[idx]->SetPos(x, y, z);
	m_Obj[idx]->SetVol(sx, sy, sz);
	m_Obj[idx]->SetColor(r, g, b, a);
	m_Obj[idx]->SetHP(hp);
	m_Obj[idx]->SetMass(vmass);
	m_Obj[idx]->SetFricoef(friccoef);
	m_Obj[idx]->SetType(type);

	return idx;
}


void ScnMgr::DeleteObject(int idx)
{
	if (idx < 0) 
	{
		std::cout << "input idx is negative: " << idx << std::endl;
		return;
	}
	if (idx >= MAX_OBJ_COUNT) 
	{
		std::cout << "input idx exceeds MAX_OBJ_COUNT: " << idx << std::endl;
		return;
	}
	if (m_Obj[idx] == NULL) 
	{
		std::cout << "m_Obj[" << idx << "] is NULL." << std::endl;
		return;
	}

	delete m_Obj[idx];
	m_Obj[idx] = NULL;
}

void ScnMgr::KeyDownInput(unsigned char key, int x, int y)
{
	if (key == 'W' || key == 'w')
	{
		m_keyW = true;
	}

	else if (key == 'A' || key == 'a')
	{
		m_keyA = true;
	}
	else if (key == 'S' || key == 's')
	{
		m_keyS = true;
	}
	else if (key == 'D' || key == 'd')
	{
		m_keyD = true;
	}
	else if (key == ' ')
	{
		m_keySP = true;
		
	}

}

void ScnMgr::KeyUpInput(unsigned char key, int x, int y)
{
	if (key == 'W' || key == 'w')
	{
		m_keyW = false;
	}

	else if (key == 'A' || key == 'a')
	{
		m_keyA = false;
	}
	else if (key == 'S' || key == 's')
	{
		m_keyS = false;
	}
	else if (key == 'D' || key == 'd')
	{
		m_keyD = false;
	}
	else if (key == ' ')
	{
		m_keySP = false;
	}

}

void ScnMgr::SpecialKeyDownInput(int key, int x, int y)
{
	/*if (key == GLUT_KEY_UP)
	{
		m_KeyUP = true;

	}

	else if (key == GLUT_KEY_DOWN)
	{
		m_KeyDOWN = true;

	}
	else if (key == GLUT_KEY_LEFT)
	{
		m_KeyLEFT = true;
	}

	else if (key == GLUT_KEY_RIGHT)
	{
		m_KeyRIGHT = true;
	}

	else if (key == GLUT_KEY_F5)
	{
		m_KeyF5 = true;
	}*/
}

void ScnMgr::SpecialKeyUpInput(int key, int x, int y)
{

	/*if (key == GLUT_KEY_UP)
	{
		m_KeyUP = false;
	}

	else if (key == GLUT_KEY_DOWN)
	{
		m_KeyDOWN = false;
	}
	else if (key == GLUT_KEY_LEFT)
	{
		m_KeyLEFT = false;
	}

	else if (key == GLUT_KEY_RIGHT)
	{
		m_KeyRIGHT = false;
	}


	else if (key == GLUT_KEY_F5)
	{
		m_KeyF5 = false;
	}*/
}


void ScnMgr::Update(float elapsedTime)
{


	float fx = 0.f;
	float fy = 0.f;
	float fz = 0.f;
	float fAmount = 20.f;

	float *cx ;
	float *cy ;
	float *cz ;

	//if (m_keyW)
	//{
	//	fy += 1;
	//}
	//if (m_keyS)
	//{
	//	fy -= 1;
	//}
	if (m_keyA)
	{
		fx -= 1;
	}

	if (m_keyD)
	{
		fx += 1;
	}
	m_Obj[HERO_ID]->MoveLimit();
	m_Obj[HERO_ID]->stopmove();
	float fSize = sqrtf(fx * fx + fy * fy);

	if (fSize > FLT_EPSILON)
	{
		fx /= fSize;
		fy /= fSize;

		fx *= fAmount;
		fy *= fAmount;

		m_Obj[HERO_ID]->
			AddForce(fx, fy, fz, elapsedTime);
	}

	if (fz > FLT_EPSILON)
	{
		fz *= fAmount * 10.f;
		float hx, hy, hz = 0.f;
		m_Obj[HERO_ID]->GetPos(&hx, &hy, &hz);
		if (hz < FLT_EPSILON)
		{
			fz *= fAmount * 30.f;
			m_Obj[HERO_ID]->AddForce(0, 0, fz, elapsedTime);
		}
	}

	//Shoot
	float bulVX, bulVY, bulVZ;
	bulVX = bulVY = bulVZ = 0.f;

	bulVY += 1.f;

	/*if (m_KeyDOWN)
	{
		bulVY -= 1.f;
	}

	if (m_KeyUP)
	{
		bulVY += 1.f;
	}

	if (m_KeyRIGHT)
	{
		bulVX += 1.f;

	}

	if (m_KeyLEFT)
	{
		bulVX -= 1.f;
	}*/


	if (m_KeyF5)
	{

		g_testAnimTex = m_Renderer->GenPngTexture("./loading.png");
	}
	float bulVSize = sqrtf(bulVX * bulVX + bulVY * bulVY + bulVZ * bulVZ);

	if (bulVSize > 0.f)
	{
		float bulletSpeed = 5.f;
		bulVX = bulletSpeed * bulVX / bulVSize;
		bulVY = bulletSpeed * bulVY / bulVSize;
		bulVZ = bulletSpeed * bulVZ / bulVSize;


		float heroX, heroY, heroZ;
		float herovX, herovY, herovZ;
		m_Obj[HERO_ID]->GetPos(&heroX, &heroY, &heroZ);
		m_Obj[HERO_ID]->GetVel(&herovX, &herovY, &herovZ);

		bulVX = bulVX + herovX;
		bulVY = bulVY + herovY;
		bulVZ = bulVZ + herovZ;


		// check whether this obj 

		if (m_Obj[HERO_ID]->CanShootBullet())
		{
			int bul = AddObject(heroX, heroY, heroZ,
				0.05, 0.05, 0.05,
				1, 0, 0, 1,
				bulVX, bulVY, bulVZ,
				10.f, 1.f, 0.6f, OBJ_BULLET);
			m_Obj[bul]->AddForce(bulVX, bulVY, bulVZ, 0.1);
			m_Obj[bul]->SetParentObj(m_Obj[HERO_ID]);
			m_Obj[HERO_ID]->ResetShootBulletCoolTime();
			
			m_Sound->PlayShortSound(g_testsoundFire, false, 1.f);

		}

		//if (m_Obj[MONSTER_ID]->CanShootBullet())
		//{
		//	int bul = AddObject(heroX, heroY, heroZ,
		//		0.05, 0.05, 0.05,
		//		1, 0, 0, 1,
		//		bulVX, bulVY, bulVZ,
		//		10.f, 1.f, 0.6f, OBJ_BULLET);
		//	m_Obj[bul]->AddForce(bulVX, bulVY, bulVZ, 0.1);
		//	m_Obj[bul]->SetParentObj(m_Obj[MONSTER_ID]);
		//	m_Obj[MONSTER_ID]->ResetShootBulletCoolTime();

		//	m_Sound->PlayShortSound(g_testsoundFire, false, 1.f);

		//}

	}

	for (int src = 0; src < MAX_OBJ_COUNT; src++)
	{
		for (int trg = 0; trg < MAX_OBJ_COUNT; trg++)
		{
			if (m_Obj[src] != NULL && m_Obj[trg] != NULL)
			{
				if (m_Phy->IsOverlap(m_Obj[src], m_Obj[trg]))
				{
					if (!m_Obj[src]->IsAncester(m_Obj[trg])
						&&
						!m_Obj[trg]->IsAncester(m_Obj[src]))
					{
						m_Phy->ProcessCollision(m_Obj[src], m_Obj[trg]);

						float srcHP, trgHP, fSrcHP, fTrgHP;
						m_Obj[src]->GetHP(&srcHP);
						m_Obj[trg]->GetHP(&trgHP);
						fSrcHP = srcHP - trgHP;
						fTrgHP = trgHP - srcHP;
						m_Obj[src]->SetHP(fSrcHP);
						m_Obj[trg]->SetHP(fTrgHP);


					}
				}




			}
		}
	}

	for (int i = 0; i < MAX_OBJ_COUNT; ++i)
	{
		if (m_Obj[i] != NULL)
		{
			m_Obj[i]->Update(elapsedTime);
		}
	}

	float x, y, z;
	m_Obj[HERO_ID]->GetPos(&x, &y, &z);
	m_Renderer->SetCameraPos(x * 30.f, y * 30.f);


}