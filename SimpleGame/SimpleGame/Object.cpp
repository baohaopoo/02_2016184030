#include "stdafx.h"
#include "Object.h"

using namespace std;

Object::Object()
{
}


Object::~Object()
{
}

void Object::SetPos(float x, float y, float z)
{
	m_posX = x;
	m_posY = y;
	m_posZ = z;

}

void Object::SetVol(float sx, float sy, float sz)
{
	m_volX = sx;
	m_volY = sy;
	m_volZ = sz;
}

void Object::SetMass(float mass)
{
	m_mass = mass;
}

void Object::SetAcc(float accX, float accY, float accZ)
{

	m_accX = accX;
	m_accY = accY;
	m_accZ = accZ;


}

void Object::SetVel(float velX, float velY, float velZ)
{
	m_velX = velX;
	m_velY = velY;
	m_velZ = velZ;

}

void Object::SetColor(float r, float g, float b, float a)
{
	m_r = r;
	m_g = g;
	m_b = b;
	m_a = a;

}

void Object::GetColor(float* r, float* g, float* b, float* a)
{
	*r = m_r;
	*g = m_g;
	*b = m_b;
	*a = m_a;
}

void Object::AddForce(float x, float y, float z, float elapsedTime)
{
	float accX = x / m_mass;
	float accY = y / m_mass;
	float accZ = z / m_mass;

	//아주 큰일이다
	m_velX = m_velX + accX * elapsedTime;
	m_velY = m_velY + accY * elapsedTime;
	m_velZ = m_velZ + accZ * elapsedTime;
}

void Object::GetPos(float* posX, float* posY, float* posZ)
{
	*posX = m_posX;
	*posY = m_posY;
	*posZ = m_posZ;
}

void Object::GetMass(float* mass)
{
	*mass = m_mass;
}

void Object::GetAcc(float* accX, float* accY, float* accZ)
{
	*accX = m_accX;
	*accY = m_accY;
	*accZ = m_accZ;
}

void Object::GetVel(float* velX, float* velY, float* velZ)
{
	*velX = m_velX;
	*velY = m_velY;
	*velZ = m_velZ;
}

void Object::GetVol(float* volX, float* volY, float* volZ)
{
	*volX = m_volX;
	*volY = m_volY;
	*volZ = m_volZ;

}

void Object::Update(float elapsedTime)
{


	//update Position

	m_posX = m_posX + m_velX * elapsedTime;
	m_posY = m_posY + m_velY * elapsedTime;
	m_posZ = m_posZ + m_velZ * elapsedTime;
}

void Object::initThis()
{
	m_posX = 0, m_posY = 0, m_posZ = 0;
	m_mass = -1.f;
	m_velX = 0, m_velY = 0, m_velZ = 0;
	m_accX = 0, m_accY = 0, m_accZ = 0;
	m_volX = 0, m_volY = 0, m_volZ = 0;
	m_r = 1.f, m_a = 1.f, m_b = 1.f, m_g = 1.f;

}

