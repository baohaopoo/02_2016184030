#include "stdafx.h"
#include "ScnMgr.h"
#include"Object.h"
#include "Dependencies\freeglut.h"
ScnMgr::ScnMgr()
{
	// Initialize Renderer
	m_Renderer = new Renderer(500, 500);
	if (!m_Renderer->IsInitialized())
	{
		std::cout << "Renderer could not be initialized.. \n";
	}

	//Initialize objects
	for (int i = 0; i < MAX_OBJ_COUNT; ++i) {
		m_Obj[i] = NULL;
	}

	m_Obj[HERO_ID] = new Object();
	m_Obj[HERO_ID]->SetPos(0, 0, 0);
	m_Obj[HERO_ID]->SetVel(0, 0, 0);
	m_Obj[HERO_ID]->SetVol(0.5, 0.5, 0.5);
	m_Obj[HERO_ID]->SetColor(1, 0, 0, 1);
	m_Obj[HERO_ID]->SetMass(50);

	

}

ScnMgr::~ScnMgr()
{
	if (m_Renderer != NULL)
	{
		delete m_Renderer;
		m_Renderer = NULL;
	}

}

void ScnMgr::RenderScene()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.0f, 0.3f, 0.3f, 1.0f);

	// Draw all m_Objs
	for (int i = 0; i < MAX_OBJ_COUNT; ++i) {
		if (m_Obj[i] != NULL) {
			float x, y, z = 0;
			float sx, sy, sz = 0;
			float r, g, b, a = 0;

			m_Obj[i]->GetPos(&x, &y, &z);

			x = 100.f * x;
			y = 100.f * y;
			z = 100.f * z;

			m_Obj[i]->GetVol(&sx, &sy, &sz);
			sx = 100.f * sx;
			sy = 100.f * sy;
			sz = 100.f * sz;

			m_Obj[i]->GetColor(&r, &g, &b, &a);


			m_Renderer->DrawSolidRect(x, y, z, sx, r, g, b, a); //z을 픽셀 단위로 변환 할 거에요 

		}
	}


}


int ScnMgr::AddObject(float x, float y, float z,
	float sx, float sy, float sz,
	float r, float g, float b, float a)
{
	int idx = -1;

	for (int i = 0; i < MAX_OBJ_COUNT; ++i) {
		if (m_Obj[i] == NULL) {
			idx = i;
			break;
		}
	}

	// 예외처리 코드

	if (idx == -1) {
		std::cout << "No more empty obj slot. " << std::endl;	//로그를 띄워준다
		return -1;	// 할당하면 안 됨 다 꽉 차서
	}

	m_Obj[idx] = new Object();
	//초당 5cm
	m_Obj[idx]->SetVel(10, 10, 10);
	m_Obj[idx]->SetPos(x, y, z);
	m_Obj[idx]->SetVol(sx, sy, sz);
	m_Obj[idx]->SetColor(r, g, b, a);

	return idx;


}

void ScnMgr::DeleteObject(int idx)
{
	if (idx < 0) {
		std::cout << "input idx is negative: " << idx << std::endl;
		return;
	}
	if (idx >= MAX_OBJ_COUNT) {
		std::cout << "input idx exceeds MAX_OBJ_COUNT: " << idx << std::endl;
		return;
	}
	if (m_Obj[idx] == NULL) {
		std::cout << "m_Obj[" << idx << "] is NULL." << std::endl;
		return;
	}

	delete m_Obj[idx];
	m_Obj[idx] = NULL;

}
void ScnMgr::Update(float elapsedTime)
{
	float fx = 0.f;
	float fy = 0.f;
	float fz = 0.f;
	float fAmount = 20.f;

	if (m_keyW)
	{
	
		 fy+= 1;
	}
	if (m_keyS)
	{
		fy -= 1;
	}
	if (m_keyA)
	{
		fx -= 1;
	}

	if (m_keyD)
	{
		fx += 1;
	}

	std::cout << "fx :" << fx << "fy: " << fy << std::endl;

	float fSize = sqrtf(fx * fx + fy * fy);

	if (fSize > FLT_EPSILON)
	{
		fx /= fSize;
		fy /= fSize;

		fx *= fAmount;
		fy *= fAmount;
		
		m_Obj[HERO_ID]->
			AddForce(fx, fy, fz, elapsedTime);
	
	}

	if (fz > FLT_EPSILON)
	{
		fz *= fAmount * 10.f;
		float hx, hy, hz = 0.f;
		m_Obj[HERO_ID]->GetPos(&hx, &hy, &hz);
		if (hz < FLT_EPSILON)
		{
			fz *= fAmount * 30.f;
			m_Obj[HERO_ID]->AddForce(0, 0, fz, elapsedTime);
		}
	}

	
	//for (int i = 0; i < 10; ++i) {
	//	m_TestChar[i] = AddObject(i * 20, i * 20, 0,
	//		20, 20, 20,
	//		1, 1, 1, 1);
	//}

	//float time = elapsedTime;
	for (int i = 0; i < MAX_OBJ_COUNT; ++i)
	{
		if (m_Obj[i] != NULL)
			m_Obj[i]->Update(elapsedTime);

	}
	float x, y, z;
	m_Obj[HERO_ID]->GetPos(&x, &y, &z);
	 


}




void ScnMgr::KeyDownInput(unsigned char key, int x, int y)
{
	if (key == 'w' || key == 'W')
	{
		m_keyW = true;
	}

	else if (key == 'a' || key == 'A')
	{
		m_keyA = true;
	}
	else if (key == 's' || key == 'S')
	{
		m_keyS = true;
	}
	else if (key == 'd' || key == 'D')
	{
		m_keyD = true;
	}

	else if (key == ' ')
	{
		m_keySP = true;
	}
}

void ScnMgr::KeyUpInput(unsigned char key, int x, int y)
{
	if (key == 'w' || key == 'W')
	{
		m_keyW = false;
	}

	else if (key == 'a' || key == 'A')
	{
		m_keyA = false;
	}
	else if (key == 's' || key == 'S')
	{
		m_keyS = false;
	}
	else if (key == 'd' || key == 'D')
	{
		m_keyD = false;
	}

	else if (key == ' ')
	{
		m_keySP = false;
	}
}
void ScnMgr::SpecialKeyDownInput(int key, int x, int y)
{
	if (key == GLUT_KEY_UP)
	{
		m_KeyUP = true;

	}

	else if (key == GLUT_KEY_DOWN)
	{
		m_KeyDOWN = true;

	}
	else if (key == GLUT_KEY_LEFT)
	{
		m_KeyLEFT = true;
	}

	else if (key == GLUT_KEY_RIGHT)
	{
		m_KeyRIGHT = true;
	}


}
void ScnMgr::SpecialKeyUpInput(int key, int x, int y)
{
	if (key == GLUT_KEY_UP)
	{
		m_KeyUP = false;
	}

	else if (key == GLUT_KEY_DOWN)
	{
		m_KeyDOWN = false;
	}
	else if (key == GLUT_KEY_LEFT)
	{
		m_KeyLEFT = false;
	}

	else if (key == GLUT_KEY_RIGHT)
	{
		m_KeyRIGHT = false;
	}
}

