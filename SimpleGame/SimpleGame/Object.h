#pragma once
class Object
{
public:
	Object();
	~Object();

	
	void SetPos(float x, float y, float z);
	void SetVol(float sx, float sy, float sz);
	void SetMass(float mass);
	void SetAcc(float accX, float accY, float accZ);
	void SetVel(float velX, float velY, float velZ);

	void SetColor(float r, float g, float b, float a);
	void GetColor(float* r, float* g, float* b, float* a);

	void AddForce(float x, float y, float z, float elapsedTime);

	//update
	void Update(float elapsedTime);
	void initThis();

public:


	void GetPos(float* posX, float* posY, float* posZ);
	void GetMass(float* mass);
	void GetAcc(float* accX, float* accY, float* accZ);
	void GetVel(float* velX, float* velY, float* velZ);
	void GetVol(float* volX, float* volY, float* volZ);


private:
	
	float m_r, m_g, m_b, m_a; // color
	float m_posX, m_posY, m_posZ; // location
	float m_mass;
	float m_velX, m_velY, m_velZ;
	float m_accX, m_accY, m_accZ;
	float m_volX, m_volY, m_volZ;


};

