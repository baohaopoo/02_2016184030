/*
Copyright 2017 Lee Taek Hee (Korea Polytech University)

This program is free software: you can redistribute it and/or modify
it under the terms of the What The Hell License. Do it plz.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY.
*/

#include "stdafx.h"
#include <iostream>
#include "Dependencies\glew.h"
#include "Dependencies\freeglut.h"

#include "Renderer.h"
#include "ScnMgr.h"

ScnMgr* g_ScnMgr = NULL;
int g_pretimeInMillisecond = 0;

void RenderScene(int elapsedTIme)
{
	int currentTime = glutGet(GLUT_ELAPSED_TIME);
	elapsedTIme =
		glutGet(GLUT_ELAPSED_TIME) - g_pretimeInMillisecond;

	g_pretimeInMillisecond = currentTime;
	//std::cout << "elapsedTime:" << elapsedTIme << std::endl;
	g_ScnMgr->Update((float)elapsedTIme / 1000.f);
	g_ScnMgr->RenderScene();

	
	glutSwapBuffers();
	glutTimerFunc(16, RenderScene, 16);
}

void Idle(void)
{
	
}

void MouseInput(int button, int state, int x, int y)
{

}

void KeyDownInput(unsigned char key, int x, int y)
{
	g_ScnMgr->KeyDownInput(key, x, y);

}
void KeyUpInput(unsigned char key, int x, int y)
{
	g_ScnMgr->KeyUpInput(key, x, y);

}

void SpecialKeyInput(int key, int x, int y)
{

}

int main(int argc, char **argv)
{
	// Initialize GL things
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(0, 0);
	glutInitWindowSize(500, 500);
	glutCreateWindow("Game Software Engineering KPU");

	glutGet(GLUT_ELAPSED_TIME);

	glewInit();
	if (glewIsSupported("GL_VERSION_3_0"))
	{
		std::cout << " GLEW Version is 3.0\n ";
	}
	else
	{
		std::cout << "GLEW 3.0 not supported\n ";
	}

	g_ScnMgr = new ScnMgr();

	glutDisplayFunc(Idle);
	glutIdleFunc(Idle);
	glutKeyboardFunc(KeyDownInput);
	glutKeyboardFunc(KeyUpInput);

	glutMouseFunc(MouseInput);
	g_pretimeInMillisecond = glutGet(GLUT_ELAPSED_TIME);

	glutTimerFunc(16, RenderScene, 16);

	glutMainLoop();

	delete g_ScnMgr;

    return 0;

}

