#pragma once

#include "Renderer.h"
#include"Object.h"
#include "Globals.h"


class ScnMgr
{
public : 
	ScnMgr();
	~ScnMgr();

	void RenderScene();
	void Update(float elapsedTime);

	int AddObject(float x, float y, float z,
		float sx, float sy, float sz,
		float r, float g, float b, float a);

	void DeleteObject(int idx);

	void KeyDownInput(unsigned char key, int x, int y);
	void KeyUpInput(unsigned char key, int x, int y);

	void SpecialKeyDownInput(int key, int x, int y);
	void SpecialKeyUpInput(int key, int x, int y);

private:
	Renderer* m_Renderer = NULL;

	Object* m_Obj[MAX_OBJ_COUNT];


	bool m_keyW = false;
	bool m_keyA = false;
	bool m_keyS = false;
	bool m_keyD = false;
	bool m_keySP = false;


	bool m_KeyUP = false;
	bool m_KeyDOWN = false;
	bool m_KeyLEFT = false;
	bool m_KeyRIGHT = false;

};

