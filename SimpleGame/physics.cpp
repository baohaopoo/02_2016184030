#include "stdafx.h"
#include "Physics.h"


Physics::Physics()
{
}


Physics::~Physics()
{
}

bool Physics::IsOverlap(Object * A, Object *B, int type)
{
	switch (type)
	{
	case 0:
		//BBOberLapTest
		return BBOberLapTest(A, B);
		break;

	case 1:
		break;

	}
	return false;
}

bool Physics::BBOberLapTest(Object * A, Object *B)
{
	//obj A 
	float aX, aY, aZ;
	float aMinX, aMinY, aMinZ;
	float aMaxX, aMaxY, aMaxZ;
	float aSX, aSY, aSZ;

	//obj B
	float bX, bY, bZ;
	float bMinX, bMinY, bMinZ;
	float bMaxX, bMaxY, bMaxZ;
	float bSX, bSY, bSZ;

	//cal box A
	A->GetPos(&aX, &aY, &aZ);
	A->GetVol(&aSX, &aSY, &aSZ);
	aMinX = aX - aSX / 2.f;	aMaxX = aX + aSX / 2.f;
	aMinY = aY - aSY / 2.f;	aMaxY = aY + aSY / 2.f;
	aMinZ = aZ - aSZ / 2.f;	aMaxZ = aZ + aSZ / 2.f;
	//cal box B
	B->GetPos(&bX, &bY, &bZ);
	B->GetVol(&bSX, &bSY, &bSZ);
	bMinX = bX - bSX / 2.f;	bMaxX = bX + bSX / 2.f;
	bMinY = bY - bSY / 2.f;	bMaxY = bY + bSY / 2.f;
	bMinZ = bZ - bSZ / 2.f;	bMaxZ = bZ + bSZ / 2.f;

	if (aMinX > bMaxX)
		return false;
	if (aMaxX < bMinX)
		return false;

	if (aMinY > bMaxY)
		return false;
	if (aMaxY < bMinY)
		return false;

	if (aMinZ > bMaxZ)
		return false;
	if (aMaxZ < bMinZ)
		return false;


	return true;
}


void Physics::ProcessCollision(Object * A, Object *B)
{
	//A,B
	float aMass, aVX, aVY, aVZ;
	A->GetMass(&aMass);
	A->GetVel(&aVX, &aVY, &aVZ);

	float bMass, bVX, bVY, bVZ;
	B->GetMass(&bMass);
	B->GetVel(&bVX, &bVY, &bVZ);

	//final vel
	float afvx, afvy, afvz;
	float bfvx, bfvy, bfvz;

	afvx = ((aMass - bMass) / (aMass + bMass))*aVX
		+ ((2.f*bMass) / (aMass + bMass)) * bVX;
	afvy = ((aMass - bMass) / (aMass + bMass))*aVY
		+ ((2.f*bMass) / (aMass + bMass)) * bVY;
	afvz = ((aMass - bMass) / (aMass + bMass))*aVZ
		+ ((2.f*bMass) / (aMass + bMass)) * bVZ;

	bfvx = ((2.f*aMass) / (aMass + bMass))*aVX
		+ ((bMass - aMass) / (aMass + bMass))*bVX;
	bfvy = ((2.f*aMass) / (aMass + bMass))*aVY
		+ ((bMass - aMass) / (aMass + bMass))*bVY;
	bfvz = ((2.f*aMass) / (aMass + bMass))*aVZ
		+ ((bMass - aMass) / (aMass + bMass))*bVZ;

	A->SetVel(afvx, afvy, afvz);
	B->SetVel(bfvx, bfvy, bfvz);
}