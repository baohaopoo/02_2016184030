#include "stdafx.h"
#include "math.h"
#include "Object.h"
#include "physics.h"
#include "ScnMgr.h"

using namespace std;

Object::Object()
{
	//Physics();
	m_remainingBulletCoolTime = m_defaultBulletCoolTime;


}


Object::~Object()
{
	m_r = 0.f, m_g = 0.f, m_b = 0.f, m_a = 1.f,
	m_posX = 0.f, m_posY = 0.f, m_posZ = 0.f,
	m_mass = 0.f,
	m_velX = 0.f, m_velY = 0.f, m_velZ = 0.f,
	m_accX = 0.f, m_accY = 0.f, m_accZ = 0.f,
	m_volX = 0.f, m_volY = 0.f, m_volZ = 0.f;
}

void Object::SetPos(float x, float y, float z)
{
	m_posX = x;
	m_posY = y;
	m_posZ = z;
}



void Object::SetVol(float sx, float sy, float sz)
{
	m_volX = sx;
	m_volY = sy;
	m_volZ = sz;
}

void Object::SetMass(float mass)
{
	m_mass = mass;
}

void Object::SetAcc(float accX, float accY, float accZ)
{
	m_accX = accX;
	m_accY = accY;
	m_accZ = accZ;
}

void Object::SetVel(float velX, float velY, float velZ)
{


	m_velX = velX;
	m_velY = velY;
	m_velZ = velZ;
}

void Object::SetColor(float r, float g, float b, float a)
{
	m_r = r;
	m_g = g;
	m_b = b;
	m_a = a;

}

void Object::SetType(int type)
{
	m_type = type;
}

void Object::GetColor(float* r, float* g, float* b, float* a)
{
	*r = m_r;
	*g = m_g;
	*b = m_b;
	*a = m_a;
}

void Object::AddForce(float x, float y, float z, float elapsedTime)
{
	float accX = x / m_mass;
	float accY = y / m_mass;
	float accZ = z / m_mass;

	m_velX = m_velX + accX * elapsedTime;
	m_velY = m_velY + accY * elapsedTime;
	m_velZ = m_velZ + accZ * elapsedTime;
}

bool Object::CanShootBullet()
{
	if (m_remainingBulletCoolTime < 0.000000001f)
	{
		return true;
	}
	else
	{
		return false;
	}
}


void Object::ResetShootBulletCoolTime()
{
	m_remainingBulletCoolTime = m_defaultBulletCoolTime;
}

void Object::Update(float elapsedTime)
{

	//reduce bullet cooltime
	m_remainingBulletCoolTime -= elapsedTime;

	//apply friction
	float Fn = GRAVITY * m_mass; //수직항력의크기
	float Friction = m_frictCoef * Fn; //마찰력의크기
 
	float dx, dy, dz; //direction
	//예외처리 추가
	float velSize = sqrtf(
		m_velX * m_velX +
		m_velY * m_velY +
		m_velZ * m_velZ);

	if (velSize > FLT_EPSILON)
	{
		dx = m_velX / velSize;
		dy = m_velY / velSize;
		dz = m_velZ / velSize;

		//calculation friction force
		float frictionX = -dx * Friction;
		float frictionY = -dy * Friction;

		//calculate  fv
		float accX = frictionX / m_mass;
		float accY = frictionY / m_mass;
		float accZ = -GRAVITY;

		//update velocity by friction force
		float newvelX = m_velX + accX * elapsedTime;
		float newvelY = m_velY + accY * elapsedTime;
		float newvelZ = m_velZ + accZ * elapsedTime;


		if (newvelX *m_velX < 0.f)
		{
			m_velX = 0.f;
		}
		else
		{
			m_velX = newvelX;
		}

		if (newvelY *m_velY < 0.f)
		{
			m_velY = 0.f;

		}
		else
		{
			m_velY = newvelY;
		}
		m_velZ = newvelZ;

	}
	else if (m_posZ > FLT_EPSILON)
	{
		float accZ = -GRAVITY;
		float newVelZ = m_velZ + accZ * elapsedTime;
		m_velZ = newVelZ;
	}
	else
	{
		m_velX = 0.f;
		m_velY = 0.f;
		m_velY = 0.f;
	}

	m_posX = m_posX + m_velX * elapsedTime;
	m_posY = m_posY + m_velY * elapsedTime;
	m_posZ = m_posZ + m_velZ * elapsedTime;

	if (m_posZ < 0.f)
	{
		m_velZ = 0.f;
		m_posZ = 0.f;
	}


}


void Object::GetTexID(int *id)
{
	*id = m_texID;
}

void Object::SetTexID(int id)
{
	m_texID = id;
}

void Object::SetParentObj(Object *obj)
{
	m_parent = m_parent;
}

Object* Object::GetParentObj()
{
	return m_parent;
}

bool Object::IsAncester(Object *obj)
{
	if (m_parent != NULL)
	{
		if (obj == m_parent)
		{
			return true;
		}
	}
	return false;
}

void Object::initThis()
{
	m_posX = 0, m_posY = 0, m_posZ = 0;
	m_mass = -1.f;
	m_velX = 0, m_velY = 0, m_velZ = 0;
	m_accX = 0, m_accY = 0, m_accZ = 0;
	m_volX = 0, m_volY = 0, m_volZ = 0;
	m_r = 1.f, m_a = 1.f, m_b = 1.f, m_g = 1.f;
	m_frictCoef = 0.f;
}

void Object::GetFricoef(float * Fricoef)
{
	*Fricoef = m_frictCoef;
}

void Object::SetFricoef(float Fricoef)
{
	m_frictCoef = Fricoef;
}

void Object::shootBullet(int type, int x, int y)
{
	if (type == 1) {
		Object* bullet = new Object();
		bullet->SetPos(150, 100, 0);
	}
	if (type == 2)
	{
		Object* bullet = new Object();
		bullet->SetPos(150, 100, 0);

	}
}

void Object::GetPos(float * posX, float * posY, float * posZ)
{
	*posX = m_posX;
	*posY = m_posY;
	*posZ = m_posZ;
}

void Object::GetMass(float * mass)
{
	*mass = m_mass;
}

void Object::GetAcc(float * accX, float * accY, float * accZ)
{
	*accX = m_accX;
	*accY = m_accY;
	*accZ = m_accZ;
}

void Object::GetVel(float * velX, float * velY, float * velZ)
{
	*velX = m_velX;
	*velY = m_velY;
	*velZ = m_velZ;
}

void Object::GetVol(float* volX, float* volY, float* volZ)
{
	*volX = m_volX;
	*volY = m_volY;
	*volZ = m_volZ;

}

void Object::GetType(int* type)
{
	*type = m_type;
}

void Object::SetHP(float hp)
{
	m_healthPoint = hp;
}
void Object::GetHP(float* hp)
{
	*hp = m_healthPoint;

}
void Object::Damage(float damage)
{
	m_healthPoint -= damage;
}

void Object::MoveLimit()
{
	std::cout << m_posX << "," << m_posY << std::endl;
	
}

void Object::stopmove()
{
	if (m_posX <= -3)
	{
		m_posX = -3;
	}
	else if (m_posX >= 3)
	{
		m_posX = 3;
	}

	else if (m_posY >= 2)
	{
		m_posY = 2;
	}

	else if (m_posY <= -2)
	{
		m_posY = -2;
	}
}

void Object::monstermove()
{


}
