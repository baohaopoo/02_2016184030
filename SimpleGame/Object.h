#pragma once
#include "Globals.h"
class Object
{
public:
	Object();
	~Object();

	//set func
	void SetPos(float x, float y, float z);
	void SetVol(float sx, float sy, float sz);
	void SetMass(float mass);
	void SetAcc(float accX, float accY, float accZ);
	void SetVel(float velX, float velY, float velZ);
	void SetColor(float r, float g, float b, float a);
	void SetType(int type);
	//get func
	void GetColor(float* r, float* g, float* b, float* a);
	void GetPos(float *posX, float *posY, float *posZ);
	void GetMass(float *mass);
	void GetAcc(float *accX, float *accY, float* accZ);
	void GetVel(float *velX, float *velY, float* velZ);
	void GetVol(float *volX, float *volY, float* volZ); //size
	void GetType(int* type);

	void SetTexID(int id);
	void GetTexID(int *id);
	//Add force (controller)	
	void AddForce(float x, float y, float z, float elapsedTime);

	//update함수
	void Update(float elapsedTime);
	void initThis();

	void GetFricoef(float* Fricoef);
	void SetFricoef(float Fricoef);

	void shootBullet(int type, int x, int y);
	
	void SetParentObj (Object *obj);
	Object* GetParentObj();
	bool IsAncester(Object *obj);

	bool CanShootBullet();
	void ResetShootBulletCoolTime();

	void SetHP(float hp);
	void GetHP(float* hp);
	void Damage(float damage);
	void MoveLimit();
	void stopmove();

	void monstermove();
private:
	// m_tInfo 
	float m_posX, m_posY, m_posZ;
	float m_mass;
	float m_velX, m_velY, m_velZ; //속도
	float m_accX, m_accY, m_accZ; //가속도
	float m_volX, m_volY, m_volZ; //부피

	float m_r, m_g, m_b, m_a; // color
	float m_frictCoef; //마찰계수
	float m_healthPoint; //체력
	float m_age;

	Object* m_parent = NULL;

	int m_type;
	int m_texID;					

	float m_remainingBulletCoolTime = 0.f;	
	float m_defaultBulletCoolTime = 0.2f;	//0.2초마다 총알 생성

};

